
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>List student</title>
    <style>
    body {
        font-family: sans-serif;
        padding: 10px;
    }

    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    .search-form {
        display: flex;
        flex-direction: column;
        width: 40%;
        padding: 10px 30px;
    }

    .form-item p {
        padding: 12px 10px 5px 10px;
        color: black;
    }

    input[type="text"] {
        width: 100%;
        border: solid 2px #007bc7;
        outline: none;
        padding: 8px;
    }

    select {
        border: solid 2px #007bc7;
        outline: none;
        width: 100%;
        padding: 8px;
    }

    input[type="submit"] {
        padding: 12px 30px;
        background-color: #3984bc;
        border-radius: 10px;
        border: solid 2px #007bc7;
        cursor: pointer;
        color: white;
        font-size: 17px;
    }

    table {
        width: 100%;
        border-collapse: separate;
        border-spacing: 0 1em;
    }

    td,
    th {
        /* border: 1px solid #dddddd; */
        text-align: left;
        padding: 6px;
    }

    form td {
        text-align: center;
    }

    .wrap-action {
        width: 90%;
        display: flex;
        justify-content: end;
    }

    .action {
        color: white;
        display: inline-block;
        padding: 10px;
        background-color: #3984bc;
        border: 1px solid #007bc7;
        text-decoration: none;
        border-radius: 10px;
        margin-right: 8px;
    }
    </style>
</head>
<?php
    session_start();
    $choice_majors = '';
    $key_word = '';

    if(isset($_SESSION["choice_majors"]) && isset($_SESSION["key_word"])){
        $choice_majors = $_SESSION["choice_majors"];
        $key_word = $_SESSION["key_word"];
    }
    if(isset($_POST['search'])){
        if(isset($_POST['choice_majors']) && isset($_POST['key_word'])) {
            $choice_majors = $_POST['choice_majors'];
            $key_word = $_POST['key_word'];
            $_SESSION["choice_majors"] = $_POST['choice_majors'];
            $_SESSION["key_word"] = $_POST['key_word'];
        }
    }
    elseif(isset($_POST['delete'])){
        $choice_majors = '';
        $key_word = '';
        $_SESSION["choice_majors"] = '';
        $_SESSION["key_word"] = '';
    }
?>
<body>
    <div class="search-form">
        <form name="registerForm" method="POST" enctype="multipart/form-data" action="searchdata.php">
            <table>
                <tr class="form-item">
                    <td>
                        <p>
                            <label for="spec">Khoa</label>
                        </p>
                    </td>
                    <td>
                        <select name="choice_majors" id="spec">
                            <?php
                                $spec = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                                if($choice_majors==''){
                                    $industryCodeArr=array("EMPTY"=>"","MAT"=>"Khoa học máy tính","KDL"=>"Khoa học vật liệu");
                                    foreach($industryCodeArr as $x=>$x_value){
                                        echo"<option>".$x_value."</option>";
                                    }
                                }else{
                                    echo "<option>".$choice_majors."</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="form-item">
                        <p>
                            <label for="spec">
                                Từ khóa
                            </label>
                        </p>
                    </td>
                    <td>
                        <input type="text" name="key_word" value="<?php echo $key_word; ?>">
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <input type="submit" name="delete" value="Xóa">
                        <input type="submit" name="search" value="Tìm kiếm">

                    </td>
                </tr>
            </table>
        </form>
    </div>
    <div class="result-search">
        <span>Số sinh viên tìm thấy:</span>
        <span>XXX</span>
    </div>
    <div class="list-student">
        <div class="wrap-action">
            <a class="action" href="./register.php">Thêm</a>
        </div>
        <div class="table-student">
            <table>
                <colgroup>
                    <col span="1" style="width: 10%;">
                    <col span="1" style="width: 25%;">
                    <col span="1" style="width: 50%;">
                    <col span="1" style="width: 15%;">
                </colgroup>
                <tr>
                    <th>No</th>
                    <th>Tên sinh viên</th>
                    <th>Khoa</th>
                    <th>Action</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Nguyễn Văn A</td>
                    <td>Khoa học máy tính</td>
                    <td>
                        <a class="action" href="">Xóa</a>
                        <a class="action" href="">Sửa</a>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Trần Thị B</td>
                    <td>Khoa học máy tính</td>
                    <td>
                        <a class="action" href="">Xóa</a>
                        <a class="action" href="">Sửa</a>
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Nguyễn Hoàng C</td>
                    <td>Khoa học vật liệu</td>
                    <td>
                        <a class="action" href="">Xóa</a>
                        <a class="action" href="">Sửa</a>
                    </td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Đinh Quang D</td>
                    <td>Khoa học vật liệu</td>
                    <td>
                        <a class="action" href="">Xóa</a>
                        <a class="action" href="">Sửa</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>

</html>